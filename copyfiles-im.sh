#!/bin/bash

: ${tarCmd:="/bin/tar"}
: ${path_root:="/"}
declare -a fileArray

tarFile="/tmp/TECHOPS-21529-$(/bin/hostname).tgz"

files=$(echo etc/sudoers \
etc/pam.d/ \
etc/group \
etc/security/pwquality.conf \
etc/security/access.conf \
etc/sssd/sssd.conf \
etc/sysconfig/authconfig \
etc/facter/facts.d/ha_ad_auth.yaml \
etc/passwd \
etc/login.defs \
etc/ssh/sshd_config )

for file in $files;
do
	fullpath=${path_root}${file}
	if [ -e ${fullpath} ]; then 
		fileArray+=(${file});
		#echo "File ${fullpath} does exist";
	else 
		echo "File ${fullpath} does not exist";
	fi
done

cd ${path_root}
${tarCmd} -zf ${tarFile} -C ${path_root} -c ${fileArray[@]}