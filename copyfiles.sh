
#!/bin/bash

: ${tarCmd:="/bin/tar"}
declare -a fileArray

tarFile="/tmp/TECHOPS-21529-$(/bin/hostname).tgz"

fileArray+=("etc/sudoers")
fileArray+=("etc/pam.d/")
fileArray+=("etc/group")
if [ -e /etc/security/pwquality.conf ]; then 
	fileArray+=("etc/security/pwquality.conf")
else echo "File does not exist"; 
fi

fileArray+=("etc/security/access.conf")
if [ -e /etc/sssd/sssd.conf ]  ; then 
	fileArray+=("etc/sssd/sssd.conf");
else echo "File does not exist"; 
fi
fileArray+=("etc/sysconfig/authconfig")
if [ -e /etc/facter/facts.d/ha_ad_auth.yaml ]; then 
	fileArray+=("etc/facter/facts.d/ha_ad_auth.yaml");
else echo "File does not exist";
fi

fileArray+=("etc/passwd")
fileArray+=("etc/login.defs")

cd /
${tarCmd} -zf ${tarFile} -C / -c ${fileArray[@]}
