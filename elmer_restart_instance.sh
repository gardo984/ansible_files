#!/bin/bash

appenv="api-operations-ecommerce-sxprd1"
if [ -f "/etc/init.d/${appenv}" ]
then
	/etc/init.d/${appenv} stop && sleep 15 && /etc/init.d/${appenv} start
else
	/bin/systemctl stop ${appenv}.service && sleep 15 && /bin/systemctl start ${appenv}.service
fi