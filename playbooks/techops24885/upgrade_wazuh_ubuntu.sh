#!/bin/bash

# uninstall agent and reinstall it
sudo apt-get remove wazuh-agent
sudo apt-get purge wazuh-agent
sudo apt-get install curl apt-transport-https lsb-release
sudo curl -s https://packages.wazuh.com/key/GPG-KEY-WAZUH | apt-key add -
echo "deb https://packages.wazuh.com/3.x/apt/ stable main" | sudo tee /etc/apt/sources.list.d/wazuh.list
apt-get update

sudo apt-get install wazuh-agent --allow-unauthenticated
sudo sed -i "s/^deb/#deb/" /etc/apt/sources.list.d/wazuh.list
echo "wazuh-agent hold" | sudo dpkg --set-selections

# finally 
sudo /var/ossec/bin/agent-auth -m ossec-server.mgmt.aws.away.black -I any
sudo /var/ossec/bin/ossec-control restart