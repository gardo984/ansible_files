#!/bin/bash

computers=$(cat ./computers.txt)
for host in $computers;
do 
	#echo ${host}
	scp -o StrictHostKeyChecking=no \
		-o ConnectTimeout=5 -o PasswordAuthentication=no \
		malazo@${host}:/tmp/TECHOPS-21529-* /Users/malazo/security_files/
done