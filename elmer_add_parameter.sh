#!/bin/bash

ticket="##TECHOPS-24986"
file="/usr/local/homeaway/api-operations-ecommerce-sxprd1/ops/env/api-operations-ecommerce-production-ops.properties"
if [ -f ${file} ]
then
	list="feature.flags.value-added-service.sdk.key=sdk-b4c984cc-391e-4664-b9d9-bbccd7c90110"
	param=$(echo ${list}|cut -d '=' -f1)
	x=$(egrep -i "${param}" ${file}|wc -l)
	if [ ${x} -eq 0 ]
	then
		printf "${ticket}\n${list}" >> ${file}
	fi
fi